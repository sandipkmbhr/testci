node 'nutanlap.local'{
#include jenkins
include myrepo
include myweb
#include pxe
include razor
package { 'puppet-server':
  ensure => latest,
}

service { 'puppetmaster':
ensure => running,
enable => true,
}
service { 'iptables':
ensure => stopped,
enable => false,
}


cron { 'resolvecorrect':
ensure  => present,
command => '/bin/cp -f /root/resolv.conf /etc/resolv.conf',
user    => 'root',
minute  => '*/1',
}

cron { 'gitmove':
ensure  => absent,
command => '/bin/bash /root/gitPUSH.sh',
user    => 'root',
minute  => '*/10',
}

package { ['rubygem-puppet-lint','epel-release','binutils','qt','gcc','make','patch','libgomp','glibc-headers','glibc-devel','kernel-headers','kernel-devel','dkms','VirtualBox-5.1','mrepo','gitk','ruby-devel','rubygems','rpm-build']:
ensure => installed,
}

file { '/etc/rc.d/rc.local':
ensure  => present,
content => 'VBoxManage list vms > /dev/null 2>&1',
}

service { 'docker':
ensure => 'running',
enable => true,
}

user { 'sandipk':
ensure => present,
uid    => 500,
gid    => [500,492],
}

file {'/tmp/hieratest':
ensure  => present,
content => hiera('sys::owner')
}
############################
$config_hash = {
  'ES_JAVA_OPTS' => '-Xms256m -Xmx256m',
  'datadir' => '/var/lib/elasticsearch-data',
}


class { 'elasticsearch':
ensure                  =>absent,
repo_version            => '5.x',
java_install            => false,
java_package            => 'java-1.8.0-openjdk',
manage_repo             => true,
init_defaults           => $config_hash,
api_protocol            => 'http',
api_host                => 'localhost',
api_port                => 9200,
api_timeout             => 10,
api_basic_auth_username => 'sandipk',
api_basic_auth_password => '$6$aGglfSxNf9vlhJfq$QGDsQUx5UwKJJ.kOLXT9e4753d7/vTigscyHawG85iTfMVMnoELAWu0cj4NIpZJft2bN1S6zsxTQm9dSby87V.',
api_ca_file             => undef,
api_ca_path             => undef,
validate_tls            => true,
}
elasticsearch::instance { 'sk-es-01':
ensure =>absent,
}
############################
# testng facter #

file { '/tmp/osfacter':
ensure  =>present,
content => "${::osfamily} , ${::architecture}",
}

#curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl

exec { 'ytd':
command => '/usr/bin/curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl',
unless  => '/usr/bin/test -f /usr/local/bin/youtube-dl',
}

file { '/usr/local/bin/youtube-dl':
ensure  => present,
mode    => '0755',
require => Exec['ytd'],
}

}
